package com.argo.documentReaderREST;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DocumentReaderRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(DocumentReaderRestApplication.class, args);
    }

}
