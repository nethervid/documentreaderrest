package com.argo.documentReaderREST.component;

import org.apache.poi.EmptyFileException;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;

public class DocxFileReader extends FileReader {

    private XWPFDocument fileReader;

    public DocxFileReader() {}
    public DocxFileReader(File file) throws IOException {
        super(file);
    }

    @Override
    public int getNumberOfPages() {
        return this.fileReader.getProperties().getExtendedProperties().getUnderlyingProperties().getPages();
    }

    @Override
    public void open() throws IOException {
        try {
            this.fileReader = new XWPFDocument(new FileInputStream(file.getAbsolutePath().toLowerCase()));
        } catch (EmptyFileException e) {
            throw new IOException(e);
        }

    }

    @Override
    public void close() throws IOException {
        fileReader.close();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocxFileReader that = (DocxFileReader) o;
        return Objects.equals(fileReader, that.fileReader);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileReader);
    }
}
