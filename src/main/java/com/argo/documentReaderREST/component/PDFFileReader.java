package com.argo.documentReaderREST.component;

import com.itextpdf.text.exceptions.InvalidPdfException;
import com.itextpdf.text.pdf.PdfReader;
import org.apache.poi.EmptyFileException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;

public class PDFFileReader extends FileReader {
    private PdfReader fileReader;

    public PDFFileReader() {}

    public PDFFileReader(File file) throws IOException {
        super(file);
    }

    @Override
    public int getNumberOfPages() {
        return fileReader.getNumberOfPages();
    }

    @Override
    public void open() throws IOException {
        try {
            this.fileReader = new PdfReader(new FileInputStream(file.getAbsolutePath()));
        } catch (InvalidPdfException | EmptyFileException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void close() {
        fileReader.close();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PDFFileReader that = (PDFFileReader) o;
        return Objects.equals(fileReader, that.fileReader);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileReader);
    }
}
