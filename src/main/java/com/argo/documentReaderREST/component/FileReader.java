package com.argo.documentReaderREST.component;

import java.io.File;
import java.io.IOException;

public abstract class FileReader implements AutoCloseable {
    protected File file;

    public FileReader() {}
    public FileReader(File file) throws IOException {
        setFile(file);
        open();
    }

    public void setFile(File file) {
        this.file = file;
    }
    public abstract int getNumberOfPages();
    public abstract void open() throws IOException;
    public abstract void close() throws IOException;
}