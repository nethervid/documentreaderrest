package com.argo.documentReaderREST.component;

import org.apache.poi.EmptyFileException;
import org.apache.poi.hwpf.HWPFDocument;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Objects;

public class DocFileReader extends FileReader {

    private HWPFDocument fileReader;

    public DocFileReader() {}
    public DocFileReader(File file) throws IOException {
        super(file);
    }

    @Override
    public int getNumberOfPages() {
        return fileReader.getSummaryInformation().getPageCount();
    }

    @Override
    public void open() throws IOException {
        try {
            this.fileReader = new HWPFDocument(new FileInputStream(this.file.getAbsolutePath()));
        } catch (EmptyFileException e) {
            throw new IOException(e);
        }
    }

    @Override
    public void close() throws IOException {
        fileReader.close();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DocFileReader that = (DocFileReader) o;
        return Objects.equals(fileReader, that.fileReader);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileReader);
    }
}
