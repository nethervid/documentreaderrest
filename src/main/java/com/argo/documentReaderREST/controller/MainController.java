package com.argo.documentReaderREST.controller;

import com.argo.documentReaderREST.service.FileService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@RestController
public class MainController {

    @GetMapping("/")
    public String greeting(Map<String, Object> model) {
        return "greeting";
    }

    @GetMapping("/readDirectory")
    @ResponseBody
    public ResponseEntity<Object> readDirectory(@RequestParam(name = "path") String path) {
        Map<String, Integer> map = new HashMap<>();

        try {
            FileService filesService = new FileService(path);
            map.put("NumberOfFiles", filesService.getNumberOfFiles());
            map.put("NumberOfPages", filesService.getNumberOfAllPages());
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}