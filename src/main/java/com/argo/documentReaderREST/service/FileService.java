package com.argo.documentReaderREST.service;

import com.argo.documentReaderREST.component.FileReader;
import com.argo.documentReaderREST.factory.FileReaderFactory;
import com.argo.documentReaderREST.factory.IFileReaderFactory;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class FileService {
    private List<File> files;

    private final IFileReaderFactory fileReaderFactory;

    public FileService(String directory) {
        files = new ArrayList<>();
        fileReaderFactory = new FileReaderFactory();
        this.initiateFileList(directory);
    }

    public int getNumberOfAllPages() {
        int numberOfPages = 0;

        for (File file : this.files) {
            try (FileReader fileReader = fileReaderFactory.createReader(file)) {
                numberOfPages += fileReader.getNumberOfPages();
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
        return numberOfPages;
    }

    public int getNumberOfFiles() {
        return files.size();
    }

    private void initiateFileList(String directory) {
        File file = new File(directory);

        if(file.isFile()) {
            this.addFileToList(file);
        } else if (file.isDirectory()) {
            this.parseDirectory(file);
        }
    }

    private void parseDirectory(File dir) {
        File[] listOfFolders = dir.listFiles(File::isDirectory);
        File[] listOfFiles = dir.listFiles(File::isFile);

        if (listOfFiles != null) {
            for (File file : listOfFiles) {
                this.addFileToList(file);
            }
        }

        if (listOfFolders != null) {
            for (File folder : listOfFolders) {
                this.parseDirectory(folder);
            }
        }
    }

    private void addFileToList(File file) {
        if (isValid(file)) {
            files.add(file);
        }
    }

    private boolean isValid(File file) {
        if (!file.exists()) {
            System.err.println(file.getAbsolutePath() + ": path not corrected");
            return false;
        }

        try {
            if (fileReaderFactory.getFileType(file) == null) {
                throw new RuntimeException(file.getAbsolutePath() + ": extension '" + FilenameUtils.getExtension(file.getName()) + "' is not supported");
            }
        } catch (IOException | RuntimeException e) {
            System.err.println(e.getMessage());
            return false;
        }

        if (!file.canRead()) {
            System.err.println(file.getAbsolutePath() + ": cannot read this file");
            return  false;
        }

        if (file.length() == 0) {
            System.err.println(file.getAbsolutePath() + ": File is empty");
            return false;
        }

        try {
            fileReaderFactory.createReader(file).close();
        } catch (IOException e) {
            System.err.println(file.getAbsolutePath() + ": cannot read this file");
            return false;
        }

        return true;
    }
}
