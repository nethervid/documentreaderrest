package com.argo.documentReaderREST.factory;

import com.argo.documentReaderREST.component.FileReader;

import java.io.File;
import java.io.IOException;

public interface IFileReaderFactory {
    public FileReader createReader(File file) throws IOException, RuntimeException;
    public FileTypeEnum getFileType(File file) throws IOException;
}