package com.argo.documentReaderREST.factory;

import com.argo.documentReaderREST.component.DocFileReader;
import com.argo.documentReaderREST.component.DocxFileReader;
import com.argo.documentReaderREST.component.FileReader;
import com.argo.documentReaderREST.component.PDFFileReader;

public enum FileTypeEnum {
    DOC("doc", "application/msword", new DocFileReader()),
    DOCX("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", new DocxFileReader()),
    PDF("pdf", "application/pdf", new PDFFileReader());

    private final String extension;
    private final String mimeType;
    private final FileReader instance;

    FileTypeEnum(String extension, String mimeType, FileReader instance) {
        this.extension = extension;
        this.mimeType = mimeType;
        this.instance = instance;
    }

    public String getExtension() { return this.extension; }
    public String getMimeType() { return this.mimeType; }
    public FileReader getInstance() { return this.instance; }
}
