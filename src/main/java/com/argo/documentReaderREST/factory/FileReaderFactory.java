package com.argo.documentReaderREST.factory;

import com.argo.documentReaderREST.component.FileReader;
import org.apache.commons.io.FilenameUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

public class FileReaderFactory implements IFileReaderFactory {

    public FileReader createReader(File file) throws IOException {
        try {
            FileTypeEnum fileTypeEnum = this.getFileType(file);
            FileReader fileReader = fileTypeEnum.getInstance();
            fileReader.setFile(file);
            fileReader.open();
            return fileReader;
        } catch (Exception e) {
            throw new IOException(file.getAbsolutePath() + ": cannot create reader for this file");
        }
    }

    public FileTypeEnum getFileType(File file) throws IOException {
        String extension = FilenameUtils.getExtension(file.getName()),
               mimeType = Files.probeContentType(Path.of(file.getAbsolutePath()));

        FileTypeEnum typeEnum = null;
        for (FileTypeEnum value : FileTypeEnum.values()) {
            if(value.getExtension().equals(extension) && value.getMimeType().equals(mimeType)) {
                typeEnum = value;
                break;
            }
        }
        return typeEnum;
    }
}