package com.argo.documentReaderREST;

import com.argo.documentReaderREST.component.FileReader;
import com.argo.documentReaderREST.component.DocFileReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class DocFileReaderTest {

    private DocFileReader fileReader;

    @Before
    public void setUp() throws IOException {
        this.fileReader = new DocFileReader(DataTest.docFile);
    }

    @Test
    public void successCreatedConstructor() {
        try {
            FileReader fileReader = new DocFileReader(DataTest.docFile);
            fileReader.close();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void failCreatedConstructor() {
        org.junit.Assert.assertThrows(IOException.class, () -> new DocFileReader(DataTest.txtFile));
    }

    @Test
    public void openStreamSuccess() {
        try (FileReader fileReaderTest = new DocFileReader()) {
            fileReaderTest.setFile(DataTest.docFile);
            fileReaderTest.open();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void openStreamFailure() {
        FileReader fileReaderTest = new DocFileReader();
        fileReaderTest.setFile(DataTest.txtFile);
        org.junit.Assert.assertThrows(IOException.class, fileReaderTest::open);
    }

    @Test
    public void closeStreamSuccess() {
        try {
            FileReader fileReaderTest = new DocFileReader();
            fileReaderTest.setFile(DataTest.docFile);
            fileReaderTest.open();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }
    @Test
    public void closeStreamFailure() {
        FileReader fileReaderTest = new DocFileReader();
        org.junit.Assert.assertThrows(NullPointerException.class, fileReaderTest::close);
    }

    @Test
    public void getNumberOfPages() throws IOException {
        int expected = 6;
        int actual = fileReader.getNumberOfPages();

        Assert.assertEquals(expected, actual);
    }
}
