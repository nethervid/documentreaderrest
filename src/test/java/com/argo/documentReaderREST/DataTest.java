package com.argo.documentReaderREST;

import java.io.File;

public class DataTest {
    public static final File docFile = new File("src/test/resources/directoryForTests/folder2/6_pages_old_format.doc");
    public static final File docxFile = new File("src/test/resources/directoryForTests/folder1/18_pages.docx");
    public static final File pdfFile = new File("src/test/resources/directoryForTests/folder1/folder1.1/pdf_18_pages.pdf");
    public static final File txtFile = new File("src/test/resources/directoryForTests/folder2/folder2.1/text.txt");
    public static final String path = "src/test/resources/directoryForTests";
    public static final String pathToFile = "src/test/resources/directoryForTests/folder1/folder1.1/pdf_18_pages.pdf";
}
