package com.argo.documentReaderREST;

import com.argo.documentReaderREST.component.FileReader;
import com.argo.documentReaderREST.component.DocFileReader;
import com.argo.documentReaderREST.component.DocxFileReader;
import com.argo.documentReaderREST.component.PDFFileReader;
import com.argo.documentReaderREST.factory.FileReaderFactory;
import com.argo.documentReaderREST.factory.FileTypeEnum;
import com.argo.documentReaderREST.factory.IFileReaderFactory;
import org.junit.Test;
import org.springframework.util.Assert;

import java.io.IOException;

public class FileReaderFactoryTest {
    private final IFileReaderFactory fileReaderFactory = new FileReaderFactory();

    @Test
    public void getFileReaderFactoryDOC() {
        try (FileReader actual = fileReaderFactory.createReader(DataTest.docFile);) {
            Assert.isInstanceOf(DocFileReader.class, actual);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getFileReaderFactoryDOCX() {
        try (FileReader actual = fileReaderFactory.createReader(DataTest.docxFile);) {
            Assert.isInstanceOf(DocxFileReader.class, actual);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getFileReaderFactoryPDF() {
        try (FileReader actual = fileReaderFactory.createReader(DataTest.pdfFile);) {
            Assert.isInstanceOf(PDFFileReader.class, actual);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getFileReaderFactoryNotSupportedType() {
        org.junit.Assert.assertThrows(IOException.class, () -> fileReaderFactory.createReader(DataTest.txtFile));
    }

    @Test
    public void getFileTypeDOC() {
        try {
            FileTypeEnum expected = fileReaderFactory.getFileType(DataTest.docFile);
            org.junit.Assert.assertEquals(FileTypeEnum.DOC, expected);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getFileTypeDOCX() {
        try {
            FileTypeEnum expected = fileReaderFactory.getFileType(DataTest.docxFile);
            org.junit.Assert.assertEquals(FileTypeEnum.DOCX, expected);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getFileTypePDF() {
        try {
            FileTypeEnum expected = fileReaderFactory.getFileType(DataTest.pdfFile);
            org.junit.Assert.assertEquals(FileTypeEnum.PDF, expected);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void getFileTypeNull() {
        try {
            FileTypeEnum expected = fileReaderFactory.getFileType(DataTest.txtFile);
            org.junit.Assert.assertNull(expected);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
