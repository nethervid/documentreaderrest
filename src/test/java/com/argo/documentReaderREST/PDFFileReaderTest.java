package com.argo.documentReaderREST;

import com.argo.documentReaderREST.component.PDFFileReader;
import com.argo.documentReaderREST.component.FileReader;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class PDFFileReaderTest {
    
    private PDFFileReader fileReader;
    
    @Before
    public void setUp() throws IOException {
        this.fileReader = new PDFFileReader(DataTest.pdfFile);
    }

    @Test
    public void successCreatedConstructor() {
        try {
            FileReader fileReader = new PDFFileReader(DataTest.pdfFile);
            fileReader.close();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void failCreatedConstructor() {
        Assert.assertThrows(IOException.class, () -> new PDFFileReader(DataTest.txtFile));
    }

    @Test
    public void openStreamSuccess() {
        try (FileReader fileReaderTest = new PDFFileReader()) {
            fileReaderTest.setFile(DataTest.pdfFile);
            fileReaderTest.open();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void openStreamFailure() {
        FileReader fileReaderTest = new PDFFileReader();
        fileReaderTest.setFile(DataTest.txtFile);
        org.junit.Assert.assertThrows(IOException.class, fileReaderTest::open);
    }

    @Test
    public void closeStreamSuccess() {
        try {
            FileReader fileReaderTest = new PDFFileReader();
            fileReaderTest.setFile(DataTest.pdfFile);
            fileReaderTest.open();
        } catch (IOException e) {
            Assert.fail(e.getMessage());
        }
    }
    @Test
    public void closeStreamFailure() {
        FileReader fileReaderTest = new PDFFileReader();
        org.junit.Assert.assertThrows(NullPointerException.class, fileReaderTest::close);
    }

    @Test
    public void getNumberOfPages() throws IOException {
        int expected = 18;
        int actual = fileReader.getNumberOfPages();
        
        Assert.assertEquals(expected, actual);
    }
}
