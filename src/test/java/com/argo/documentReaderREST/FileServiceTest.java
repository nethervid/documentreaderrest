package com.argo.documentReaderREST;

import com.argo.documentReaderREST.service.FileService;
import org.junit.Assert;
import org.junit.Test;


public class FileServiceTest  {

    @Test
    public void constructorWithBrokenPath() {
        FileService fileService = new FileService("C:\\random\\123");

        int expected = 0;
        int actual = fileService.getNumberOfFiles();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void constructorPathToFile() {
        FileService fileService = new FileService(DataTest.pathToFile);

        int expected = 1;
        int actual = fileService.getNumberOfFiles();
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getNumberOfPages() {
        FileService fileService = new FileService(DataTest.path);

        int expected = 108;
        int actual = fileService.getNumberOfAllPages();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void getNumberOfFiles() {
        FileService fileService = new FileService(DataTest.path);

        int expected = 9;
        int actual = fileService.getNumberOfFiles();

        Assert.assertEquals(expected, actual);
    }
}
